const userServices = require('../services/user.service');

async function getUser(request, response) {
    const result = await userServices.getUser(request);

    return response.status(result.code).json({
        success: result.status,
        data: result.data,
    });
}

async function addUser(request, response) {
    const result = await userServices.addUser(request);
    return response.status(result.code).json({
        success: result.status,
        message: result.message,
        data: result.data,
    });
}

async function editUser(request, response) {
    try {
        // const result = await editUser(request);
        // return response.status(result.data.code).json({
        //     success: result.status,
        //     message: result.message,
        //     data: result.data,
        // });
    } catch (error) {
        console.log(error);
        return response.status(500).json({
            success: false,
            message: 'Internal Server Error',
        });
    }
}

async function deleteUser(request, response) {
    try {
        //const resut = await deleteUser(request);
        // return response.status(result.data.code).json({
        //     success: result.status,
        //     message: result.message,
        //     data: result.data,
        // });
    } catch (error) {
        console.log(error);
        return response.status(500).json({
            success: false,
            message: 'Internal Server Error',
        });
    }
}

module.exports = {
    getUser,
    addUser,
    editUser,
    deleteUser,
};
