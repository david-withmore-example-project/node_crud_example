const { Router, request } = require('express');

//* import all controllers in here
const userController = require('../controllers/user.controller');

//* import all middlewares in here
const validateData = require('../middleware/validate.data');

//* import try catch remover
const tryCatchRemover = require('../utils/try_catch.remover');
const router = Router();

router.get('/', tryCatchRemover(userController.getUser));
router.post('/', tryCatchRemover(validateData.validateInfo), tryCatchRemover(userController.addUser));

module.exports = router;
