function errorMiddleware(error, request, response, next) {
    console.log(error);

    return response.status(500).json({
        success: false,
        message: 'Internal Server Error',
    });
}

module.exports = errorMiddleware;
