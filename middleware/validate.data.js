function validateInfo(request, response, next) {
    if (Object.keys(request.body).length === 0) {
        return response.status(400).json({
            success: false,
            message: 'empty Data!',
        });
    } else {
        console.log(request.body);
        return next();
    }
}

module.exports = { validateInfo };
