global.data = [
    { id: 1, name: 'David' },
    { id: 2, name: 'David' },
    { id: 3, name: 'David' },
];

async function getUser(request, response) {
    return {
        code: 200,
        status: true,
        data,
    };
}

async function addUser(request, response) {
    data.push(request.body);
    return {
        code: 201,
        status: true,
        message: 'user has been added!',
        data: request.body,
    };
}

async function editUser(request, response) {}

async function deleteUser(request, response) {}

module.exports = {
    getUser,
    addUser,
    editUser,
    deleteUser,
};
