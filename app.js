const express = require('express');
const cors = require('cors');
const { config } = require('dotenv');
const morgan = require('morgan');
const helmet = require('helmet');
const exceptionHandler = require('./middleware/exception.handler');

//* fetch environment variables in .env using dotenv config
config();

//* get port number in .env file
const PORT = process.env.PORT;

//* import all routes in here
const userRoutes = require('./routes/user.routes');

//* instatiate express library to be used for boostraping.
const app = express();

//* enable express json middleware to enable json parsing
app.use(express.json());

//*  enable express urlenconded to enable form data
app.use(express.urlencoded({ extended: true }));

//* enable basic security like preventing xss injection or hide framework
app.use(helmet.xssFilter());
app.use(helmet.hidePoweredBy());
app.use(helmet.noSniff());

//* enable cors for allowing all origin to fetch api from all origin
app.use(cors());

//* use exceptionHandler middleware for global handler
app.use(exceptionHandler);

// * use imported routes to enable routing functionality
app.use('/user', userRoutes);

//* start the server using listen method
app.listen(PORT, () => console.log(`server is listening on port: ${PORT}`));
