function tryCatchRemover(handler) {
    return async (request, response, next) => {
        try {
            await handler(request, response);
        } catch (error) {
            return next(error);
        }
    };
}

module.exports = tryCatchRemover;
